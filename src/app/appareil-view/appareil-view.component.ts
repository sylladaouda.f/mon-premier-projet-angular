import { Subscription } from 'rxjs';
import { AppareilService } from './../service/appareil.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-appareil-view',
  templateUrl: './appareil-view.component.html',
  styleUrls: ['./appareil-view.component.css']
})
export class AppareilViewComponent implements OnInit, OnDestroy {

  estAuth = false;

  dernierMAJ = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(() => {
      resolve(date);
    }, 2000);
  })


  mesAppareils : any[];
  appareilSouscription : Subscription;

  constructor(private appareilService : AppareilService) {
    setTimeout(() => {
      this.estAuth = true;
    }, 4000);
  }

  ngOnInit(): void {
    //this.mesAppareils = this.appareilService.mesAppareils;
    this.appareilSouscription = this.appareilService.appareilSubject.subscribe((value : any[]) =>{
      this.mesAppareils = value;
    });

    this.appareilService.emettreAppareilsSubject();
  }

  allumer() {
    this.appareilService.toutAllumer();
  }

  eteindre(){
    this.appareilService.toutEteindre();
  }

  onSave(){
    this.appareilService.saveAppareilToServer();
  }

  onFetch(){
    this.appareilService.getAppareilToServer();
  }

  ngOnDestroy(): void {
    this.appareilSouscription.unsubscribe();
  }
}
