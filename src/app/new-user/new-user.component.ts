import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { UserService } from '../service/user.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  //Correspond au formulaire dans le template
  userForm : FormGroup;

  constructor(private formBuilder : FormBuilder, private userService : UserService, private router : Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    this.userForm = this.formBuilder.group({
      prenom : ["", Validators.required],
      nom : ["", Validators.required],
      email : ["", [Validators.required, Validators.email]],
      drinkPreference : ["", Validators.required],
      hobbies : this.formBuilder.array([])
    })
  }

  ajouterUser(){
    const formValue = this.userForm.value;
    const newUser = new User(formValue["prenom"],
                             formValue["nom"],
                             formValue["email"],
                             formValue["drinkPreference"],
                             formValue["hobbies"] ? formValue["hobbies"] : []);
    this.userService.ajouterUser(newUser);
    this.router.navigate(["users"]);
  }

  getHobbies(){
    return this.userForm.get('hobbies') as FormArray;
  }

  onAddHobbie(){
    const newHobbieControl = this.formBuilder.control('', Validators.required);
    this.getHobbies().push(newHobbieControl);
  }
}
