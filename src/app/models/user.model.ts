export class User{
    constructor(public prenom : string,
                public nom : string,
                public email : string,
                public drinkPreference : string,
                public hobbies?: string[]){}
}