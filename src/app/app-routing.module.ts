import { UserListComponent } from './user-list/user-list.component';
import { AjouterAppareilComponent } from './ajouter-appareil/ajouter-appareil.component';
import { ErreurComponent } from './erreur/erreur.component';
import { DetailAppareilComponent } from './detail-appareil/detail-appareil.component';
import { AuthComponent } from './auth/auth.component';
import { AppareilViewComponent } from './appareil-view/appareil-view.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './service/auth-guard.service';
import { NewUserComponent } from './new-user/new-user.component';

const routes: Routes = [
  {
    path : "appareils", 
    canActivate : [AuthGuard],
    component : AppareilViewComponent
  },

  {
    path : "appareils/:id",
    canActivate : [AuthGuard],
    component : DetailAppareilComponent
  },

  {
    path : "auth",
    component : AuthComponent
  },

  {
    path : "",
    component : AppareilViewComponent
  },

  {
    path : "ajouter", 
    canActivate : [AuthGuard],
    component : AjouterAppareilComponent
  },

  {
    path : "users",
    component : UserListComponent
  },

  {
    path : "new-user",
    component : NewUserComponent
  },

  {
    path : "not-found",
    component : ErreurComponent
  },

  {
    path : "**",
    redirectTo : "/not-found"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
