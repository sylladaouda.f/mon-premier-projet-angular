import { UserService } from './../service/user.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../models/user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {

  users : User[];
  userSouscription : Subscription;
  constructor(private userService : UserService) { }

  ngOnInit() {
    this.userSouscription = this.userService.userSubject.subscribe((users : User[]) => {
      this.users = users;
    });
    this.userService.emitUsers();
  }

  /**
   * destruction de souscription quand le component se détruit
   */
  ngOnDestroy(): void {
    this.userSouscription.unsubscribe();
  }

}
