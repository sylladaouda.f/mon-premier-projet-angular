import { AuthService } from './../service/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  statutAth : boolean;

  constructor(private authService : AuthService, private router : Router) { }

  ngOnInit() {
    this.statutAth = this.authService.isAuth;
  }

  connecter(){
    this.authService.connecter().then(
      ()=> {
        this.statutAth = this.authService.isAuth;
        this.router.navigate(['appareils'])
      });
  }

  deconnecter(){
    this.authService.exit().then(
      ()=> {
        this.statutAth = this.authService.isAuth;
      });
  }
}
