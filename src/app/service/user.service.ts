import { Subject } from 'rxjs';
import { User } from './../models/user.model';
export class UserService{
    private users : User[] = [
        {
            prenom : "Fatim",
            nom : "Doumbia",
            email : "fatim.doumbia@fre.fr",
            drinkPreference : "Bouye",
            hobbies : ["Danser", "Dormir"]
        }
    ];

    //Un observable subjet permettant d'émettre des array de type user
    userSubject = new Subject<User[]>();

    emitUsers(){
        this.userSubject.next(this.users.slice());
    }

    ajouterUser(user : User){
        this.users.push(user);
        this.emitUsers();
    }
}