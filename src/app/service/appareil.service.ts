import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppareilService {

  constructor(private http : HttpClient) { }

  appareilSubject = new Subject<any[]>();
  private mesAppareils = [
    // {
    //   id : '1',
    //   nom : 'Ordinateur',
    //   statut : 'Allumé'
    // },
    // {
    //   id : '2',
    //   nom : 'Smartphone',
    //   statut : 'Eteint'
    // },
    // {
    //   id : '3',
    //   nom : 'Télévision',
    //   statut : 'Eteint'
    // }
  ];


  emettreAppareilsSubject(){
    this.appareilSubject.next(this.mesAppareils.slice());
  }

  toutAllumer(){
    this.mesAppareils.forEach(appareil => {
      appareil.statut = "Allumé";
    });

    this.emettreAppareilsSubject();
  }

  toutEteindre(){
    this.mesAppareils.forEach(appareil => {
      appareil.statut = "Eteint";
    });

    this.emettreAppareilsSubject();
  }

  allumer(index : number){
    this.mesAppareils[index].statut = "Allumé";

    this.emettreAppareilsSubject();
  }

  eteindre(index : number){
    this.mesAppareils[index].statut = "Eteint";

    this.emettreAppareilsSubject();
  }

  appareilParId(id : String){
    const monAppareil = this.mesAppareils.find(
      (appareil) => {
        return appareil.id === id;
      });
    return monAppareil;
  }

  ajouterAppareil(nom : string, statut : string){
    const appareilObjet = {
      id : '0',
      nom : '',
      statut : ''
    }
    appareilObjet.nom = nom;
    appareilObjet.statut = statut;
    appareilObjet.id = this.mesAppareils[this.mesAppareils.length - 1].id + 1;
    this.mesAppareils.push(appareilObjet);
    this.emettreAppareilsSubject();
  }

  saveAppareilToServer(){
    this.http
    .put('https://fir-ed3a6.firebaseio.com/appareils.json', this.mesAppareils)
    .subscribe(() => {
      console.log('Enregistrement OK');
      
    },
    (erreur) =>{
      console.log('Erreur lors de l\'enregistrement ', erreur);
    })
  }

  getAppareilToServer(){
    this.http
    .get<any[]>('https://fir-ed3a6.firebaseio.com/appareils.json')
    .subscribe((response) => {
      this.mesAppareils = response;
      this.emettreAppareilsSubject();
    },
    (erreur) =>{
      console.log('Erreur lors de la récupération ', erreur);
    })
  }
}
