export class AuthService{
    isAuth = false;

    connecter(){
        return new Promise(
            (resolve, reject) => {

            setTimeout(() => {
               this.isAuth = true;
               resolve(true);
            }, 2000);
        });
    }

    exit(){
        return new Promise(
            (resolve, reject) => {

            setTimeout(() => {
               this.isAuth = false;
               resolve(true);
            }, 2000);
        });
    }
}