import { AppareilService } from './../service/appareil.service';
import { Component, Input, OnInit } from '@angular/core';

const STATUT_ETEINT = "Eteint";
const STATUT_ALLUME = "Allumé";

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.css']
})
export class AppareilComponent implements OnInit {

  @Input() public nomAppareil: String;
  @Input() public statutAppareil: String;
  @Input() public indexAppareil: number;
  @Input() public idAppareil: number;
  

  constructor(private appareilService : AppareilService) { }

  ngOnInit() {
  }

  getStatus() {
    return this.statutAppareil;
  }

  isEteint(){
    return this.getStatus() === STATUT_ETEINT;
  }

  isAllume(){
    return this.getStatus() === STATUT_ALLUME;
  }

  allumer(){
    this.appareilService.allumer(this.indexAppareil); 
  }

  eteindre(){
    this.appareilService.eteindre(this.indexAppareil);
  }

}
