import { interval, Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
  
  secondes : number = 0;
  compterSouscription : Subscription;

  ngOnInit(): void {

    // const compter = interval(1000);
    // this.compterSouscription = compter.subscribe((value: number) =>{
    //   this.secondes = value;
    // },
    // (error : any) => {
    //   console.log("Une erreur attention");
    // },
    // () => {
    //   console.log("Observavble terminé");
    // });
  }

  constructor(){}

   //Destruction de la souscription
   ngOnDestroy(): void {
    this.compterSouscription.unsubscribe();
  }

}
