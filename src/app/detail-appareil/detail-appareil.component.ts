import { AppareilService } from './../service/appareil.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-appareil',
  templateUrl: './detail-appareil.component.html',
  styleUrls: ['./detail-appareil.component.css']
})
export class DetailAppareilComponent implements OnInit {

  nom : String = "Appareil";
  statut : String = "Statut";
  constructor(private appareilService : AppareilService, private route : ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.nom = this.appareilService.appareilParId(id).nom;
    this.statut = this.appareilService.appareilParId(id).statut;
  }

  

}
