import { Router } from '@angular/router';
import { AppareilService } from './../service/appareil.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-ajouter-appareil',
  templateUrl: './ajouter-appareil.component.html',
  styleUrls: ['./ajouter-appareil.component.css']
})
export class AjouterAppareilComponent implements OnInit {

  statutParDefaut = "Eteint";

  constructor(private appareilService : AppareilService, private router : Router) { }

  ngOnInit() {
  }

  enregistrer(form : NgForm){
    const nom = form.value["nom"];
    const statut = form.value["statut"];

    this.appareilService.ajouterAppareil(nom, statut);
    this.router.navigate(["appareils"]);
  }

}
