import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterAppareilComponent } from './ajouter-appareil.component';

describe('AjouterAppareilComponent', () => {
  let component: AjouterAppareilComponent;
  let fixture: ComponentFixture<AjouterAppareilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterAppareilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterAppareilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
